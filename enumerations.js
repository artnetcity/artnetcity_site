'use strict';

exports = module.exports = function(app) {
    var Enum = require('enum');
  
    var enums = [];
    
    var enumsDir = "schema/enumerations";
    
    var normalizedPath = require("path").join(__dirname, enumsDir);
    
    require("fs").readdirSync(normalizedPath).forEach(function(file) {
        enums.push(file.slice(0, file.indexOf('.')));
    });
    
    enums.forEach(function (name) {
        var items = require('./schema/enumerations/' + name)();
        app.locals.enumerations[name] = new Enum(items, { freez: true }); 
    });
};
