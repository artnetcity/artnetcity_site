'use strict';

exports = module.exports = function(app, mongoose) {
  var settingsSchema = new mongoose.Schema({
    user: {
      id: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
      name: { type: String, default: '' }
    },
    language: { type: String, default: 'en' },
    address: { type: String, default: '' },
    pageView: {
      rightBlocks: [String],
      leftMenu: {
        edit: { type: Boolean, default: true },
        settings: { type: Boolean, default: true },
        professions: { type: Boolean, default: true },
        contacts: { type: Boolean, default: true },
        news: { type: Boolean, default: true },
        messages: { type: Boolean, default: true },
        images: { type: Boolean, default: true },
        articles: { type: Boolean, default: true },
        video: { type: Boolean, default: true },
        audio: { type: Boolean, default: true },
        goods: { type: Boolean, default: true },
        services: { type: Boolean, default: true },
        orders: { type: Boolean, default: true },
        rating: { type: Boolean, default: true }
      }
    },
    trade: {
      stopGoods: { type: Boolean, default: false },
      stopServices: { type: Boolean, default: false },
      hideNameForComment: { type: Boolean, default: false },
      hideGoodsForComments: { type: Boolean, default: false }
    },
    blacklist: [String],
    privacy: {
      showMainInformation: { type: String, default: 'all' },
      showComments: { type: String, default: 'all' },
      showMediaLine: { type: String, default: 'all' },
      allowComments: { type: String, default: 'all' },
      allowMessages: { type: String, default: 'all' },
      showWall: { type: String, default: 'all' },
      allowWallComments: { type: String, default: 'all' },
      showOtherWallComments: { type: String, default: 'all' },
      allowContactOrders: { type: String, default: 'all' },
      hidePage: { type: String, default: 'nobody' },
      
      showPersonalImages: { type: String, default: 'all' },
      showProfessionalImages: { type: String, default: 'all' },
      showPersonalAudio: { type: String, default: 'all' },
      showProfessionalAudio: { type: String, default: 'all' },
      showPersonalVideo: { type: String, default: 'all' },
      showProfessionalVideo: { type: String, default: 'all' },
      showPersonalArticles: { type: String, default: 'all' },
      showProfessionalArticles: { type: String, default: 'all' }
    },
    search: [String]
  });
  
  settingsSchema.plugin(require('./plugins/pagedFind'));
  settingsSchema.index({ user: 1 });
  settingsSchema.index({ search: 1 });
  settingsSchema.set('autoIndex', (app.get('env') === 'development'));
  
  app.db.model('Settings', settingsSchema);
};
