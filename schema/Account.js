'use strict';

exports = module.exports = function(app, mongoose) {
  var accountSchema = new mongoose.Schema({
    user: {
      id: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
      name: { type: String, default: '' }
    },
    isVerified: { type: String, default: '' },
    verificationToken: { type: String, default: '' },
    name: {
      first: { type: String, default: '' },
      middle: { type: String, default: '' },
      last: { type: String, default: '' },
      full: { type: String, default: '' }
    },
    company: { type: String, default: '' },
    phone: { type: String, default: '' }, phoneSecond: { type: String, default: '' },
    zip: { type: String, default: '' },
    sex: { type: String, default: '' },
    maritalStatus: { type: String, default: '' }, showMaritalStatus: { type: String, default: '' },
    birthday: { type: Date, default: '' }, showBirthday: { type: String, default: '' }, showBirthdayOnlyMonthAndDay: { type: String, default: '' },
    birthplaceCountry: { type: String, default: '' }, birthplaceCity: { type: String, default: '' },
    contactsCountry: { type: String, default: '' }, contactsCity: { type: String, default: '' }, contactsAddress: { type: String, default: '' }, contactsShowAddress: { type: String, default: '' },
    skype: { type: String, default: '' }, site: { type: String, default: '' },
    languages: [String],
    schools: [{
      name: { type: String },
      yearBegin: { type: String, default: '' },
      yearEnd: { type: String, default: '' },
      country: { type: String, default: '' },
      city: { type: String, default: '' }
    }],
    coleges: [{
      name: { type: String },
      yearEnd: { type: String, default: '' },
      specialty: { type: String },
      status: { type: String, default: '' },
      country: { type: String, default: '' },
      city: { type: String, default: '' }
    }],
    works: [{
      place: { type: String },
      position: { type: String, default: '' },
      country: { type: String, default: '' },
      city: { type: String, default: '' },
      dateBegin: { type: Date, default: '' },
      dateEnd: { type: Date, default: '' },
    }],
    creativity: { type: String, default: '' },
    sourcesOfInspiration: [String],
    music: { type: String }, movies: { type: String }, books: { type: String }, games: { type: String },
    theMainThingInLife: { type: String }, importantInOthers: { type: String }, badHabits: { type: String }, religion: { type: String }, politics: { type: String },
    quotations: { type: String }, aboutMe: { type: String },
    status: {
      id: { type: String, ref: 'Status' },
      name: { type: String, default: '' },
      userCreated: {
        id: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
        name: { type: String, default: '' },
        time: { type: Date, default: Date.now }
      }
    },
    statusLog: [mongoose.modelSchemas.StatusLog],
    notes: [mongoose.modelSchemas.Note],
    userCreated: {
      id: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
      name: { type: String, default: '' },
      time: { type: Date, default: Date.now }
    },
    search: [String]
  });
  
  accountSchema.plugin(require('./plugins/pagedFind'));
  accountSchema.index({ user: 1 });
  accountSchema.index({ 'status.id': 1 });
  accountSchema.index({ search: 1 });
  accountSchema.set('autoIndex', (app.get('env') === 'development'));
  
  app.db.model('Account', accountSchema);
};
