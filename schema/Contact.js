'use strict';

exports = module.exports = function(app, mongoose) {
  var contactSchema = new mongoose.Schema({
    user0: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    user1: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    relation0: { type: Number, default: 0, max: 2 },
    relation1: { type: Number, default: 0, max: 2 },
  });

  contactSchema.index({ user0: 1 });
  contactSchema.index({ user1: 1 });
  contactSchema.set('autoIndex', (app.get('env') === 'development'));
  
  app.db.model('Contact', contactSchema);
};
