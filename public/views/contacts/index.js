/* global app:true */

(function() {
    'use strict';

    app = app || {};

    app.Contact = Backbone.Model.extend({
        idAttribute: '_id',
        urlRoot: '/contacts/del',
        defaults: {
            user: {}
        }
    });

    app.Contacts = Backbone.Collection.extend({
        model: app.Contact,
        url: '/contacts/list/',
        parse: function(data) {
            return data.contacts;
        }
    });
    
    app.TabContactsView = Backbone.View.extend({
        el: '#tab-contacts',
        template: _.template( $('#tmpl-contacts').html() ),
        initialize: function() {
            this.render();
        },
        render: function() {
            this.$el.html(this.template());
        }
    });

    app.TabOnlineView = Backbone.View.extend({
        el: '#tab-online',
        template: _.template( $('#tmpl-online').html() ),
        initialize: function() {
            this.render();
        },
        render: function() {
            this.$el.html(this.template());
        }
    });

    app.TabIncomingRequestsView = Backbone.View.extend({
        el: '#tab-incoming-requests',
        template: _.template( $('#tmpl-incoming-requests').html() ),
        initialize: function() {
            this.render();
        },
        render: function() {
            this.$el.html(this.template());
        }
    });
    
    app.TabOutcomingRequestsView = Backbone.View.extend({
        el: '#tab-outcoming-requests',
        template: _.template( $('#tmpl-outcoming-requests').html() ),
        initialize: function() {
            this.render();
        },
        render: function() {
            this.$el.html(this.template());
        }
    });

    app.ContactView = Backbone.View.extend({
        template: _.template( $('#tmpl-contact').html() ),
        events: {
          'click .del': 'del'
        },
        render: function() {
            var attr = this.model.toJSON();

            attr.user.work = attr.user.account.works && attr.user.account.works[0] ? attr.user.account.works[0].position : false;

            this.$el.html(this.template(attr));
            
            this.$el.find('.da-thumbs > li').hoverdir();
            
            return this;
        },
        del: function() {
            var self = this;
            this.model.destroy({success: function() {
                self.collection.fetch();
            }});
        }
    });
    
    app.ContactsView = Backbone.View.extend({
        el: '#contacts-list-contacts',
        initialize: function() {
            this.listenTo(this.collection, "update", this.render);
        },
        render: function() {
            this.$el.html("");
            this.collection.each(this.appendContact, this);
        },
        appendContact: function(contact) {
            var contactView = new app.ContactView({collection: this.collection, model: contact});
            this.$el.append(contactView.render().el);
        }
    });
    
    $(document).ready(function() {
        app.contacts = new app.Contacts();
        
        app.tabContactsView = new app.TabContactsView();
        app.tabOnlineView = new app.TabOnlineView();
        app.tabIncomingRequestsView = new app.TabIncomingRequestsView();
        app.tabOutcomingRequestsView = new app.TabOutcomingRequestsView();
        
        app.contactsView = new app.ContactsView({collection: app.contacts});

        app.contacts.fetch();
    });
}());
