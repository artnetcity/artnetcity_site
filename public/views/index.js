/* global app:true */

(function() {
  'use strict';

  app = app || {};

  app.Account = Backbone.Model.extend({
    idAttribute: '_id',
    url: '/account/settings/'
  });

  app.User = Backbone.Model.extend({
    idAttribute: '_id',
    url: '/account/settings/'
  });

  app.Contact = Backbone.Model.extend({
    idAttribute: '_id',
    defaults: {
      userId: _userId
    },
    url: '/contacts/add/'
  });
  
  app.ContactDelete = Backbone.Model.extend({
    idAttribute: '_id',
    urlRoot: '/contacts/del'
  });
  
  app.ModalAddContactView = Backbone.View.extend({
    el: '#contacts-button',
    template: _.template( $('#tmpl-contactsButton').html() ),
    events: {
      'click .btn-add-contact': 'add',
    },
    initialize: function() {
      this.model = app.indexView.contact;
      
      this.render();
    },
    render: function() {
      this.$el.html(this.template(this.model.attributes));
    },
    add: function() {
      var self = this,
        userNum = this.$el.find('[name="userNum"]').val();
        
      console.log(userNum);
      
      this.model.save({num: userNum}, {
        success: function(model, response) {
          if (response.success) {
            self.model.set(response.contact);
            self.$el.modal('toggle');
          }
        }
      });
    }
  });

  app.ContactsButtonView = Backbone.View.extend({
    el: '#contacts-button',
    template: _.template( $('#tmpl-contactsButton').html() ),
    events: {
      'click button[data-target="#delContact"]': 'del',
      'click button[data-target="#addContact"]': 'add'
    },
    initialize: function() {
      this.model = app.indexView.contact;
      this.modelDelete = new app.ContactDelete({ _id: this.model.id });

      this.listenTo(this.model, 'sync', this.render);
      this.listenTo(this.model, 'change', this.render);

      this.render();
    },
    render: function() {
      if (this.model.id) {
        this.modelDelete.set({ _id: this.model.id });
      }
      this.$el.html(this.template(this.model.attributes));
    },
    del: function() {
      var self = this;
      
      this.modelDelete.destroy({success: function(model, response) {
        self.model.clear().set(self.model.defaults);
      }});
    },
    add: function() {
      var self = this;
        
      this.model.save({num: userNum}, {
        success: function(model, response) {
          if (response.success) {
            self.model.set(response.contact);
          }
        }
      });

    }
  });
  
  app.ModalSendMessageView = Backbone.View.extend({
    el: '#modalSendMessage',
    template: _.template( $('#tmpl-modalSendMessage').html() ),
    events: {
      'click .btn-update': 'update',
    },
    initialize: function() {
      this.render();
    },
    render: function() {
      this.$el.html(this.template());
    }
  });  

  app.ModalSubscribeView = Backbone.View.extend({
    el: '#modalSubscribe',
    template: _.template( $('#tmpl-modalSubscribe').html() ),
    events: {
      'click .btn-update': 'update',
    },
    initialize: function() {
      this.render();
    },
    render: function() {
      this.$el.html(this.template());
    }
  });
  
  app.IndexView = Backbone.View.extend({
    el: '.container .content',
    initialize: function() {
      app.indexView = this;
      
      this.account = new app.Account( JSON.parse( unescape($('#data-account').html()) ) );
      this.user = new app.User( JSON.parse( unescape($('#data-user').html()) ) );
      
      var c = JSON.parse( unescape($('#data-contact').html()) );
      c.userId = _userId;
      this.contact = new app.Contact( c );
    }
  });

  $(document).ready(function() {
    app.indexView = new app.IndexView();
    
    app.modalAddContactView = new app.ModalAddContactView();
    app.modalSendMessageView = new app.ModalSendMessageView();
    app.modalSubscribeView = new app.ModalSubscribeView();
    
    app.contactsButtonView = new app.ContactsButtonView();
  });
}());
