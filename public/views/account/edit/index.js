/* global app:true */

(function() {
  'use strict';

  var accountFields = ['first', 'middle', 'last', 'company', 'phone', 'phoneSecond', 'zip', 'sex', 
    'maritalStatus', 'showMaritalStatus', 
    'birthday', 'showBirthday', 'showBirthdayOnlyMonthAndDay', 
    'birthplaceCountry', 'birthplaceCity',
    'contactsCountry', 'contactsCity', 'contactsAddress', 'contactsShowAddress',
    'skype', 'site',
    'languages',
    'schools', 'coleges',
    'works', 'creativity', 'sourcesOfInspiration'
  ];

  app = app || {};

  app.Account = Backbone.Model.extend({
    idAttribute: '_id',
    url: '/account/edit/'
  });

  app.User = Backbone.Model.extend({
    idAttribute: '_id',
    url: '/account/edit/'
  });

  app.Details = Backbone.Model.extend({
    idAttribute: '_id',
    defaults: {
      success: false,
      errors: [],
      errfor: {},
      first: '', middle: '', last: '', sex: '',
      company: '', phone: '', zip: '',
      maritalStatus: '', showMaritalStatus: 'no',
      birthday: '', showBirthday: 'no', showBirthdayOnlyMonthAndDay: 'no',
      birthplaceCountry: '', birthplaceCity: '',
      languages: []
    },
    url: '/account/edit/',
    parse: function(response) {
      if (response.account) {
        app.mainView.account.set(response.account);
        delete response.account;
      }

      return response;
    }
  });
  
  app.Education = Backbone.Model.extend({
    idAttribute: '_id',
    defaults: {
      success: false,
      errors: [],
      errfor: {},
      schools: [], coleges: [],
      schoolName: '', schoolCountry: '', schoolCity: '', schoolYearBegin: '', schoolYearEnd: '',
      colegeName: '', colegeCountry: '', colegeCity: '', colegeYearEnd: '', colegeStatus: '', colegeSpecialty: ''
    },
    url: '/account/edit/education/',
    parse: function(response) {
      if (response.education) {
        app.mainView.account.set(response.education);
        delete response.education;
      }

      return response;
    }
  });
  
  app.Activities = Backbone.Model.extend({
    idAttribute: '_id',
    defaults: {
      success: false,
      errors: [],
      errfor: {},
      works: [], work: {place: '', position: '', country: '', city: '', dateBegin: '', dateEnd: ''},
      creativity: '', sourcesOfInspiration: []
    },
    url: '/account/edit/activities/',
    parse: function(response) {
      if (response.activities) {
        app.mainView.account.set(response.activities);
        
        delete response.activities;
      }

      return response;
    }
  });

  app.Additional = Backbone.Model.extend({
    idAttribute: '_id',
    defaults: {
      success: false,
      errors: [],
      errfor: {},
      music: '', movies: '', books: '', games: '',
      theMainThingInLife: '', importantInOthers: '', badHabits: '', religion: '', politics: '',
      quotations: '', aboutMe: ''
    },
    url: '/account/edit/additional/',
    parse: function(response) {
      if (response.additional) {
        app.mainView.account.set(response.additional);
        
        delete response.additional;
      }

      return response;
    }
  });

  app.Identity = Backbone.Model.extend({
    idAttribute: '_id',
    defaults: {
      success: false,
      errors: [],
      errfor: {},
      username: '',
      email: ''
    },
    url: '/account/edit/identity/',
    parse: function(response) {
      if (response.user) {
        app.mainView.user.set(response.user);
        delete response.user;
      }

      return response;
    }
  });

  app.Password = Backbone.Model.extend({
    idAttribute: '_id',
    defaults: {
      success: false,
      errors: [],
      errfor: {},
      newPassword: '',
      confirm: ''
    },
    url: '/account/edit/password/',
    parse: function(response) {
      if (response.user) {
        app.mainView.user.set(response.user);
        delete response.user;
      }

      return response;
    }
  });

  app.DetailsView = Backbone.View.extend({
    el: '#details',
    template: _.template( $('#tmpl-details').html() ),
    events: {
      'click .btn-update': 'update'
    },
    initialize: function() {
      this.model = new app.Details();
      this.syncUp();
      this.listenTo(app.mainView.account, 'change', this.syncUp);
      this.listenTo(this.model, 'sync', this.render);
      this.render();
    },
    syncUp: function() {
      var data = {
        _id: app.mainView.account.id,
        first: app.mainView.account.get('name').first,
        middle: app.mainView.account.get('name').middle,
        last: app.mainView.account.get('name').last,
      }
      
      accountFields.forEach(function(name) {
        if (name in data) {
          return;
        }
        data[name] = app.mainView.account.get(name);
      });
      
      this.model.set(data);
    },
    render: function() {
      this.$el.html(this.template( this.model.attributes ));
      
      for (var key in this.model.attributes) {
        if (this.model.attributes.hasOwnProperty(key)) {
          var attr = this.model.attributes[key];
          var $attr = this.$el.find('[name="'+ key + (typeof(attr) === 'object' ? '[]' : '') +'"]');
          
          $attr.each(function (index) {
            var v = (typeof(attr) === 'object') && (index in attr) ? attr[index] : attr;
            
            $(this).val(v);
          });
        }
      }

      utils.postRenderView(this.$el);
    },
    update: function() {
      var self = this, 
        data = {};
      
      accountFields.forEach(function(name) {
        var $fields = self.$el.find('[name="'+name+'"],[name="'+name+'[]"]');
        
        $fields.each(function() {
          var $f = $(this);
          var n = $f.attr('name');
          var v = $f.val();
          
          if ($f.hasClass('date')) {
            v = utils.dateForServer(v, $f.data('language'));
          }
          
          if (n.indexOf('[]') === -1) {
            data[name] = v;
          } else {
            if (!(name in data)) {
              data[name] = [];
            }
            
            if (v) {
              data[name].push(v);
            }
          }
        })
      });
      
      this.model.save(data);
    }
  });

  app.EducationView = Backbone.View.extend({
    el: '#education',
    template: _.template( $('#tmpl-education').html() ),
    events: {
      'click .btn-update': 'update',
      'click .btn-add-school': 'showNewSchool',
      'click .btn-add-colege': 'showNewColege',
    },
    initialize: function() {
      this.model = new app.Education();
      this.syncUp();
      this.listenTo(app.mainView.account, 'change', this.syncUp);
      this.listenTo(this.model, 'sync', this.render);
      this.render();
    },
    syncUp: function() {
      var data = {
        _id: app.mainView.account.id,
        schools: app.mainView.account.get('schools'),
        coleges: app.mainView.account.get('coleges')
      }
      
      this.model.set(data);
    },
    render: function() {
      this.$el.html(this.template( this.model.attributes ));
      
      utils.postRenderView(this.$el);
    },
    update: function() {
      var self = this, 
        data = {schools: [], coleges: []},
        newSchool = {name: false}, newColege = {name: false};
        
      var $fields = self.$el.find('[name^="school"],[name^="colege"]');
      
      $fields.each(function() {
        var $f = $(this),
          i = parseInt($f.data('index')),
          n = $f.attr('name'),
          dn = $f.data('name'),
          v = $f.val();
          
        if (n.indexOf("school") > -1) {
          if (i == -1) {
            newSchool[dn] = v;
          } else {
            if (!data.schools[i]) {
              data.schools[i] = {};
            }
            data.schools[i][dn] = v;
          }
        } else { // colege
          if (i == -1) {
            newColege[dn] = v;
          } else {
            if (!data.coleges[i]) {
              data.coleges[i] = {};
            }
            data.coleges[i][dn] = v;
          }
        }
      });
      
      if (newSchool.name) {
        data.schools.push(newSchool);
      }
      if (newColege.name) {
        data.coleges.push(newColege);
      }
      
      this.model.save(data);
    },
    showNewSchool: function() {
      $('.btn-add-school').hide();
      $('.new-school').show();
      return false;
    },
    showNewColege: function() {
      $('.btn-add-colege').hide();
      $('.new-colege').show();
      return false;
    }
  });

  app.ActivitiesView = Backbone.View.extend({
    el: '#activities',
    template: _.template( $('#tmpl-activities').html() ),
    events: {
      'click .btn-update': 'update'
    },
    initialize: function() {
      this.model = new app.Activities();
      this.syncUp();
      this.listenTo(app.mainView.account, 'change', this.syncUp);
      this.listenTo(this.model, 'sync', this.render);
      this.render();
    },
    syncUp: function() {
      var data = {
        _id: app.mainView.account.id,
        works: app.mainView.account.get('works'),
        creativity: app.mainView.account.get('creativity'),
        sourcesOfInspiration: app.mainView.account.get('sourcesOfInspiration'),
      }
      
      if (data.works && data.works.length) {
        data.work = data.works[0];
      }
      
      this.model.set(data);
    },
    render: function() {
      this.$el.html(this.template( this.model.attributes ));
      
      utils.postRenderView(this.$el);
    },
    update: function() {
      var self = this, 
        data = {works: [], creativity: '', sourcesOfInspiration: []},
        work = {place: '', position: '', country: '', city: '', dateBegin: '', dateEnd: ''};
        
      var $fields = self.$el.find('#Work [name]');
      
      $fields.each(function() {
        var $f = $(this),
          n = $f.attr('name'),
          v = $f.val();
        
        if ($f.hasClass('date')) {
          v = utils.dateForServer(v, $f.data('language'));
        }

        work[n] = v;
      });
      
      data.creativity = self.$el.find('[name="creativity"]').val();
      
      self.$el.find('[name="sourcesOfInspiration"]').each(function() {
        var v = $(this).val();
        
        if (v) {
          data.sourcesOfInspiration.push(v);
        }
      });
      
      data.works.push(work);

      this.model.save(data);
    }
  });

  app.AdditionalView = Backbone.View.extend({
    el: '#additional',
    template: _.template( $('#tmpl-additional' ).html() ),
    events: {
      'click .btn-update': 'update'
    },
    initialize: function() {
      this.model = new app.Additional();
      this.syncUp();
      this.listenTo(app.mainView.account, 'change', this.syncUp);
      this.listenTo(this.model, 'sync', this.render);
      this.render();
    },
    syncUp: function() {
      var data = {
        _id: app.mainView.account.id,
        music: app.mainView.account.get('music'),
        movies: app.mainView.account.get('movies'),
        books: app.mainView.account.get('books'),
        games: app.mainView.account.get('games'),
        theMainThingInLife: app.mainView.account.get('theMainThingInLife'),
        importantInOthers: app.mainView.account.get('importantInOthers'),
        badHabits: app.mainView.account.get('badHabits'),
        religion: app.mainView.account.get('religion'),
        politics: app.mainView.account.get('politics'),
        quotations: app.mainView.account.get('quotations'),
        aboutMe: app.mainView.account.get('aboutMe')
      }
      
      this.model.set(data);
    },
    render: function() {
      this.$el.html(this.template( this.model.attributes ));
    },    
    update: function() {
      var self = this, 
        data = {};

      var $fields = self.$el.find('[name]');
      
      $fields.each(function() {
        var $f = $(this),
          n = $f.attr('name'),
          v = $f.val();
        
        if ($f.hasClass('date')) {
          v = utils.dateForServer(v, $f.data('language'));
        }

        data[n] = v;
      });
      
      this.model.save(data);
    }
  });

  app.IdentityView = Backbone.View.extend({
    el: '#identity',
    template: _.template( $('#tmpl-identity').html() ),
    events: {
      'click .btn-update': 'update'
    },
    initialize: function() {
      this.model = new app.Identity();
      this.syncUp();
      this.listenTo(app.mainView.user, 'change', this.syncUp);
      this.listenTo(this.model, 'sync', this.render);
      this.render();
    },
    syncUp: function() {
      this.model.set({
        _id: app.mainView.user.id,
        username: app.mainView.user.get('username'),
        email: app.mainView.user.get('email')
      });
    },
    render: function() {
      this.$el.html(this.template( this.model.attributes ));

      for (var key in this.model.attributes) {
        if (this.model.attributes.hasOwnProperty(key)) {
          this.$el.find('[name="'+ key +'"]').val(this.model.attributes[key]);
        }
      }
    },
    update: function() {
      this.model.save({
        username: this.$el.find('[name="username"]').val(),
        email: this.$el.find('[name="email"]').val()
      });
    }
  });

  app.PasswordView = Backbone.View.extend({
    el: '#password',
    template: _.template( $('#tmpl-password').html() ),
    events: {
      'click .btn-password': 'password'
    },
    initialize: function() {
      this.model = new app.Password({ _id: app.mainView.user.id });
      this.listenTo(this.model, 'sync', this.render);
      this.render();
    },
    render: function() {
      this.$el.html(this.template( this.model.attributes ));

      for (var key in this.model.attributes) {
        if (this.model.attributes.hasOwnProperty(key)) {
          this.$el.find('[name="'+ key +'"]').val(this.model.attributes[key]);
        }
      }
    },
    password: function() {
      this.model.save({
        newPassword: this.$el.find('[name="newPassword"]').val(),
        confirm: this.$el.find('[name="confirm"]').val()
      });
    }
  });

  app.MainView = Backbone.View.extend({
    el: '.page .container',
    initialize: function() {
      app.mainView = this;
      this.account = new app.Account( JSON.parse( unescape($('#data-account').html()) ) );
      this.user = new app.User( JSON.parse( unescape($('#data-user').html()) ) );

      app.detailsView = new app.DetailsView();
      app.educationView = new app.EducationView();
      app.activitiesView = new app.ActivitiesView();
      app.additionalView = new app.AdditionalView();
      
      app.identityView = new app.IdentityView();
      app.passwordView = new app.PasswordView();
    }
  });

  $(document).ready(function() {
    app.mainView = new app.MainView();
  });
}());
