/* global app:true */

(function() {
  'use strict';

  app = app || {};

  app.Locale = Backbone.Model.extend({
    url: '/locale/',
  });

  app.Address = Backbone.Model.extend({
    idAttribute: '_id',
    defaults: {
      success: false,
      errors: [],
      errfor: {},
      address: '',
    },
    url: '/account/settings/address/'
  });

  app.Settings = Backbone.Model.extend({
    idAttribute: '_id',
    defaults: {
      success: false,
      errors: [],
      errfor: {},
      language: 'en',
      num: 1,
      address: '',
      pageView: {
        rightBlocks: ['contacts', 'images', 'audio', 'video', 'articles'],
        leftMenu: {
          edit: true, settings: true, professions: true, contacts: true, news: true, messages: true, images: true, 
          articles: true, video: true, audio: true, goods: true, services: true, orders: true
        }
      },
      trade: {
        stopGoods: false, stopServices: false, hideNameForComment: false, hideGoodsForComments: false
      },
      blacklist: [],
      privacy: {
        showMainInformation: 'all',
        showComments: 'all',
        showMediaLine: 'all',
        allowComments: 'all',
        allowMessages: 'all',
        showWall: 'all',
        allowWallComments: 'all',
        showOtherWallComments: 'all',
        allowContactOrders: 'all',
        hidePage: 'all',
        
        showPersonalImages: 'all',
        showProfessionalImages: 'all',
        showPersonalAudio: 'all',
        showProfessionalAudio: 'all',
        showPersonalVideo: 'all',
        showProfessionalVideo: 'all',
        showPersonalArticles: 'all',
        showProfessionalArticles: 'all'
      },
      emailShort: "snp***@gmail.com",
      phoneShort: "+3806******86"
    },
    url: '/account/settings/',
    parse: function(response) {
      if (response.settings) {
        app.settingsView.settings.set(response.settings);
        delete response.settings;
      }

      return response;
    }
  });

  app.MainView = Backbone.View.extend({
    el: '#main',
    template: _.template( $('#tmpl-main').html() ),
    events: {
      'click .btn-update': 'update',
      'click .btn-change-password': 'changePassword',
      'click .btn-change-address': 'changeAddress',
      'click .btn-change-email': 'changeEmail',
      'click .btn-change-phone': 'changePhone',
      'change .checkbox-left-menu': 'changeLeftMenu',
      'change .checkbox-trade': 'changeTrade',
      'click .change-blacklist': 'changeBlacklist',
      'click .rightBlocks a': 'nop',
      'change [name="language"]': 'setSytemLanguage',
    },
    initialize: function() {
      this.model = app.settingsView.settings;
      this.locale = new app.Locale();
      this.address = new app.Address({address: this.model.get("address")});
      
      this.listenTo(this.model, 'sync', this.render);
      
      this.render();
    },
    render: function() {
      this.$el.html(this.template( this.model.attributes ));
      
      utils.postRenderView(this.$el);
      
      this.$el.find('.rightBlocks').sortable({
        cancel: '[data-sort="0"]',
        update: function( event, ui ) {
          app.mainView.changeRightBlocks();
        }
      });
    },
    setSytemLanguage: function() {
      var language = this.$el.find('[name="language"]').val();
      var locale = this.locale;
      
      var data = {language: language};
      
      this.model.save(data, {
        success: function(model, response) {
          if (response.success) {
            locale.save({locale: language}, {
              success: function(model, response) {
                location.assign("/account/settings/");
              }
            });
          }
        }
      });
    },
    changePassword: function() {
      var p = this.$el.find('[name="password"]').val();
      var p0 = this.$el.find('[name="newPassword0"]').val();
      var p1 = this.$el.find('[name="newPassword1"]').val();
      
      alert("changePassword " + p + p0 + p1);
    },
    changeAddress: function() {
      var self = this,
        data = {address: this.$el.find('[name="address"]').val()};

      this.address.save(data, {
        success: function(model, response) {
          if (response.success) {
            app.settingsView.settings.set('address', response.settings.address);
          } else {
            app.settingsView.settings.set('errors', response.errors);
            app.settingsView.settings.set('errfor', response.errfor);
          }
          self.render();
        }
      });
    },
    changeEmail: function() {
      var email = this.$el.find('[name="email"]').val();
      alert("changeEmail" + email);
    },
    changePhone: function() {
      var phone = this.$el.find('[name="phone"]').val();
      alert("changePhone " + phone);
    },
    changeRightBlocks: function() {
      var data = {pageView: {rightBlocks: []}};
      
      this.$el.find('.rightBlocks li').each(function() {
        var name = $(this).attr('name');
        data.pageView.rightBlocks.push(name);
      });
      
      this.model.save(data);
    },
    changeLeftMenu: function(event) {
      var $this = $(event.target),
        name = $this.attr('name'),
        val = $this.is(':checked');
        
      var data = {pageView: {leftMenu: {}}};
      data.pageView.leftMenu[name] = val;

      this.model.save(data);
    },
    changeTrade: function(event) {
      var $this = $(event.target),
        name = $this.attr('name'),
        val = $this.is(':checked');
        
      var data = {trade: {}};
      data.trade[name] = val;

      this.model.save(data);
    },
    changeBlacklist: function(e) {
      alert('changeBlacklist');
      e.preventDefault();
    },
    nop: function(e) {
      e.preventDefault();
    },
    update: function() {
      var self = this, 
        data = {};
      
      this.model.save(data);
    }
  });

  app.PrivacyView = Backbone.View.extend({
    el: '#privacy',
    template: _.template( $('#tmpl-privacy').html() ),
    events: {
      'click .btn-update': 'update'
    },
    initialize: function() {
      this.model = app.settingsView.settings;
      
      this.listenTo(this.model, 'sync', this.render);
      
      this.render();
    },
    render: function() {
      this.$el.html(this.template( this.model.attributes ));
      
      utils.postRenderView(this.$el);
    },
    update: function() {
      var self = this, 
        data = {privacy: {}};
        
      this.$el.find('.privacy').each(function() {
        var name = $(this).attr('name'),
          val = $(this).val();
          
        data.privacy[name] = val;
      });
      
      this.model.save(data);
    }
  });

  app.SettingsView = Backbone.View.extend({
    el: '.page .container',
    initialize: function() {
      app.settingsView = this;
      
      this.settings = new app.Settings( JSON.parse( unescape($('#data-settings').html()) ) );
      
      var num = JSON.parse( unescape($('#data-num').html()) );
      console.log(num);
      this.settings.set('num', num);

      app.mainView = new app.MainView();
      app.privacyView = new app.PrivacyView();
    }
  });

  $(document).ready(function() {
    app.settingsView = new app.SettingsView();
  });
}());
