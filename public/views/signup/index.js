/* global app:true */

(function() {
  'use strict';

  app = app || {};
  
  app.Signup = Backbone.Model.extend({
    url: '/signup/',
    defaults: {
      errors: [],
      errfor: {},
      username: '',
      password: '',
      firstname: '',
      lastname: '',
    }
  });
  
  app.SignupView = Backbone.View.extend({
    el: '#signup',
    template: _.template( $('#tmpl-signup').html() ),
    events: {
      'submit form': 'preventSubmit',
      'keypress [name="password"]': 'signupOnEnter',
      'click .btn-signup': 'signup'
    },
    initialize: function() { //рендерим модель c backbone
      this.model = new app.Signup();
      this.listenTo(this.model, 'sync', this.render);
      this.render();
    },
    render: function() {
      this.$el.html(this.template( this.model.attributes ));
      this.$el.find('[name="firstname"]').focus();
    },
    preventSubmit: function(event) {
      event.preventDefault();//выполняем функцию при событии
    },
    signupOnEnter: function(event) {
      if (event.keyCode !== 13) { return; }
      if ($(event.target).attr('name') !== 'password') { return; }
      event.preventDefault();
      this.signup();
    },
    signup: function() {
      this.$el.find('.btn-signup').attr('disabled', true);
      
      this.model.save({
        username: this.$el.find('[name="username"]').val(),
        password: this.$el.find('[name="password"]').val(),
        firstname: this.$el.find('[name="firstname"]').val(),
        lastname: this.$el.find('[name="lastname"]').val(),
      },{
        success: function(model, response) {
          if (response.success) {
            location.href = '/account/';
          }
          else {
            model.set(response);
          }
        }
      });
    }
  });
  
  /* присваиваем функции новое значение */
  $(document).ready(function() {
    app.signupView = new app.SignupView();
  });
}());
