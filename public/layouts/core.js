/* global app:true */
/* exported app */

var app; //the main declaration

var utils = {
  accountOutputFormatDate: 'YYYY-MM-DD',
  
  postRenderView: function($el) {
    $el.find('.btn-group').each(function() {
      var $btn = $(this).find('[name]');
      var v = $btn.val();
      
      if (v) {
        var $btnText = $(this).find('[data-value="'+ v +'"]:first');
        if ($btnText.length) {
          $btn.addClass('filled');
          $btn.find('.text').text($btnText.text());
        }
      } else {
        $btn.removeClass('filled');
      }
    });

    $el.find('input.date').each(function() {
      var params = {};
      
      var language = $(this).data('language');
      if (language != 'en') {
        params.language = language;
        
        var d = $(this).val();
        if (d) {
          var md = moment(d, utils.accountOutputFormatDate).subtract(0, 'days').calendar();

          $(this).val(md);
        }
      }

      $(this).datepicker(params);
    });
    
    
    $el.find('[data-sort]').sort(function (a, b) {
      var contentA = parseInt($(a).attr('data-sort'));
      var contentB = parseInt($(b).attr('data-sort'));
      return (contentA < contentB) ? -1 : (contentA > contentB) ? 1 : 0;
    }).appendTo($el.find('[data-sort]:first').parent());
  },
  
  dateForServer: function(date, language) {
    if (!date) {
      return '';
    }
    
    if (language && language != 'en') {
      var f = $.fn.datepicker.dates[language].format;
      date = moment(date, f.toUpperCase()).format(utils.accountOutputFormatDate);
    } else {
      date = moment(date).format(utils.accountOutputFormatDate);
    }
    
    return date;
  }
};

(function() {
  'use strict';
  
  $(document).ready(function() {
    //active (selected) navigation elements
    $('.nav [href="'+ window.location.pathname +'"]').closest('li').toggleClass('active');

    //register global ajax handlers
    $(document).ajaxStart(function(){ $('.ajax-spinner').show(); });
    $(document).ajaxStop(function(){  $('.ajax-spinner').hide(); });
    $.ajaxSetup({
      beforeSend: function (xhr) {
        xhr.setRequestHeader('x-csrf-token', $.cookie('_csrfToken'));
      }
    });

    //ajax spinner follows mouse
    $(document).bind('mousemove', function(e) {
      $('.ajax-spinner').css({
        left: e.pageX + 15,
        top: e.pageY
      });
    });
    
    $('body').on("click", ".dropdown-menu li a", function() {
      var $btnGroup = $(this).parents(".btn-group");
      var $btn = $btnGroup.find('.btn');
      var v = $btn.val();
      
      $btnGroup.find('.btn .text').text($(this).text());
      $btn.val($(this).data('value'));
      
      if (v != $(this).data('value')) {
        $btn.trigger('change');
      }
      
      $btnGroup.find('.dropdown-toggle').dropdown('toggle');
      
      return false;
    });
    
    $('body').on("change", "input[type='checkbox']", function() {
      var v = $(this).data('value');
      if (v) {
        $(this).data('value', $(this).val());
        $(this).val(v);
      }
    });
    
    $('body').on("click", ".btn-add-prev", function() {
      var $clone = $(this).prev().clone();
      var name = $(this).data('name');
      
      if (name) {
        $clone.find('[name]').attr('name', name);
      }
      
      $(this).closest('.btn-add-container').before($clone);
      
      $clone.show();
      
      return false;
    });
    
    setInterval(function(){ 
      $.get('/ping/', function(response) {
        console.log(response);
      });
    }, 10000);
  });
}());
