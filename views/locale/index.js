'use strict';

var setLocale = function(req, res, locale, url){
    res.cookie('locale', locale);

    if (url) {
        res.redirect(url);
    } else {
        var workflow = req.app.utility.workflow(req, res);
        return workflow.emit('response');
    }
};

exports.get = function(req, res){
    return setLocale(req, res, req.params.locale, "/");
};

exports.post = function(req, res){
    return setLocale(req, res, req.body.locale, req.body.backUrl);
};
