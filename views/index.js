'use strict';

var renderIndex = function(req, res, next, oauthMessage, userId) {
  var outcome = {};

  var getData = function(callback) {
    req.app.db.models.User
    .findById(userId, 'username timeCreated roles num')
    .populate('roles.account')
    .populate('roles.settings')
    .exec(function(err, user) {
      if (err) {
        callback(err, null);
      }
      
      var account = user.roles.account;
      delete user.roles.account;
      var settings = user.roles.settings;
      delete user.roles.settings;
      
      var momentjs = require('moment');
      
      var d_format = res.__('dateFormat');

      if (account.showBirthdayOnlyMonthAndDay === 'yes') {
        d_format = d_format.replace(/\.y|y/gi, "");
      }
      
      account._birthday = momentjs(account.birthday).format(d_format.toUpperCase());
      
      outcome.user = user;
      outcome.account = account;
      outcome.settings = settings;
      
      return callback(null, 'done');
    });
  };
  
  var getContact = function(callback) {
    outcome.contact = false;
    
    if (req.user && req.user.id && userId != req.user.id) {
      var n = (""+req.user.id).match(/[0-9]/g).join("");
      var n0 = (""+userId).match(/[0-9]/g).join("");

      var fields = {};
      
      fields.user0 = n > n0 ? userId : req.user._id;
      fields.user1 = n > n0 ? req.user._id : userId;
  
      req.app.db.models.Contact.findOne(fields).select('user0 user1 relation0 relation1').exec(function(err, contact) {
        if (err || !contact) {
        } else {
          outcome.contact = contact;
        }

        return callback(null, 'done');
      });
    } else {
      return callback(null, 'done');
    }
  };
  
  
  var asyncFinally = function(err, results) {
    if (err) {
      return next(err);
    }

    res.render('index', {
      data: {
        account: escape(JSON.stringify(outcome.account)),
        user: escape(JSON.stringify(outcome.user)),
        settings: escape(JSON.stringify(outcome.settings)),
        contact: escape(JSON.stringify(outcome.contact)),
      },
      user: outcome.user,
      account: outcome.account,
      settings: outcome.settings,
    });
  };

  require('async').parallel([getData, getContact], asyncFinally);
};

exports.init = function(req, res, next){
  if (!req.isAuthenticated()) {
    res.redirect('/login/');
  } else if (req.user.roles && req.user.roles.settings.address) {
    res.redirect('/' + req.user.roles.settings.address);
  } else if (req.user.num) {
    res.redirect('/' + req.user.num);
  } else {
    renderIndex(req, res, next, '', req.user.id);
  }
};

exports.id = function(req, res, next){
  var num = req.url.substr(1);
  
  req.app.db.models.User.findOne({ 'num': num }).select('num roles').populate('roles.settings').exec(function(err, user) {
      if (err || !user) {
        require('../views/http/index').http404(req, res);
      } else {
        var settings = user.roles.settings;
        if (settings && settings.address) {
          res.redirect('/' + settings.address);
        } else {
          renderIndex(req, res, next, '', user._id);
        }
      }
  });
};

exports.address = function(req, res, next){
  var address = req.url.substr(1);

  req.app.db.models.Settings.findOne({ 'address': address }).select('user').exec(function(err, settings) {
      if (err || !settings) {
        require('../views/http/index').http404(req, res);
      } else {
        renderIndex(req, res, next, '', settings.user.id);
      }
  });
};