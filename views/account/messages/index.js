'use strict';


var renderMessages = function(req, res, next, oauthMessage) {
  var outcome = {};

  var asyncFinally = function(err, results) {
    if (err) {
      return next(err);
    }
    
    res.render('account/messages/index');
  };

  require('async').parallel([], asyncFinally);
};

exports.init = function(req, res, next){
  renderMessages(req, res, next, '');
};