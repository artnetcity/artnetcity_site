'use strict';

exports.update = function(req, res, next){
  var workflow = req.app.utility.workflow(req, res);

  workflow.on('validate', function() {
    if (!req.body.schools) {
      workflow.outcome.errfor.schools = 'required';
    }

    if (!req.body.coleges) {
      workflow.outcome.errfor.schools = 'required';
    }

    if (workflow.hasErrors()) {
      return workflow.emit('response');
    }

    workflow.emit('patchEducation');
  });

  workflow.on('patchEducation', function() {
    var fieldsToSet = {
      schools: req.body.schools.filter(function (v) { return !!v.name; }),
      coleges: req.body.coleges.filter(function (v) { return !!v.name; }),
    };
    
    var options = { 'new': true, select: 'schools coleges' };

    req.app.db.models.Account.findByIdAndUpdate(req.user.roles.account.id, fieldsToSet, options, function(err, education) {
      if (err) {
        return workflow.emit('exception', err);
      }

      workflow.outcome.education = education;
      return workflow.emit('response');
    });
  });

  workflow.emit('validate');
};

