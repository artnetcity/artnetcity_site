'use strict';

exports.update = function(req, res, next){
  var workflow = req.app.utility.workflow(req, res);

  workflow.on('validate', function() {
    if (!req.body.works) {
      workflow.outcome.errfor.works = 'required';
    }

    if (!req.body.sourcesOfInspiration) {
      workflow.outcome.errfor.sourcesOfInspiration = 'required';
    }
    
    if (!('creativity' in req.body)) {
      workflow.outcome.errfor.creativity = 'required';
    }

    if (workflow.hasErrors()) {
      return workflow.emit('response');
    }

    workflow.emit('patchActivities');
  });

  workflow.on('patchActivities', function() {
    var fieldsToSet = {
      works: req.body.works.filter(function (v) { return !!v.place; }),
      creativity: req.body.creativity,
      sourcesOfInspiration: req.body.sourcesOfInspiration.filter(function (v) { return !!v; }),
    };
    
    var options = { 'new': true, select: Object.keys(fieldsToSet).join(" ") };

    req.app.db.models.Account.findByIdAndUpdate(req.user.roles.account.id, fieldsToSet, options, function(err, activities) {
      if (err) {
        return workflow.emit('exception', err);
      }

      workflow.outcome.activities = activities;
      return workflow.emit('response');
    });
  });

  workflow.emit('validate');
};

