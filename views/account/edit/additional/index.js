'use strict';

exports.update = function(req, res, next){
  var workflow = req.app.utility.workflow(req, res);

  workflow.on('validate', function() {
    //if (!req.body.schools) {
    //  workflow.outcome.errfor.schools = 'required';
    //}

    if (workflow.hasErrors()) {
      return workflow.emit('response');
    }

    workflow.emit('patchAdditional');
  });

  workflow.on('patchAdditional', function() {
    var fieldsToSet = {
      music: req.body.music,
      movies: req.body.movies,
      books: req.body.books,
      games: req.body.games,
      theMainThingInLife: req.body.theMainThingInLife,
      importantInOthers: req.body.importantInOthers,
      badHabits: req.body.badHabits,
      religion: req.body.religion,
      politics: req.body.politics,
      quotations: req.body.quotations,
      aboutMe: req.body.aboutMe,
    };
    
    var options = { 'new': true, select: Object.keys(fieldsToSet).join(" ") };

    req.app.db.models.Account.findByIdAndUpdate(req.user.roles.account.id, fieldsToSet, options, function(err, additional) {
      if (err) {
        return workflow.emit('exception', err);
      }

      workflow.outcome.additional = additional;
      return workflow.emit('response');
    });
  });

  workflow.emit('validate');
};

