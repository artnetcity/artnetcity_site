'use strict';

var settingsSelect = ['language', 'address', 'pageView', 'trade', 'blacklist', 'privacy'];

var enumSystemLanguages = require('../../../schema/enumerations/SystemLanguages')();
var enumPrivacy = require('../../../schema/enumerations/Privacy')();
var enumRightBlocks = require('../../../schema/enumerations/RightBlocks')();

var renderSettings = function(req, res, next, oauthMessage) {
  var outcome = {};

  var getSettingsData = function(callback) {
    req.app.db.models.Settings.findById(req.user.roles.settings.id, settingsSelect.join(" ")).exec(function(err, settings) {
      if (err) {
        return callback(err, null);
      }
      
      outcome.settings = settings;
      outcome.num = req.user.num;
      callback(null, 'done');
    });
  };

  var asyncFinally = function(err, results) {
    if (err) {
      return next(err);
    }
    
    res.render('account/settings/index', {
      data: {
        settings: escape(JSON.stringify(outcome.settings)),
        num: escape(JSON.stringify(outcome.num)),
      },
    });
  };

  require('async').parallel([getSettingsData], asyncFinally);
};

exports.init = function(req, res, next){
  renderSettings(req, res, next, '');
};

exports.address = function(req, res, next){
  var workflow = req.app.utility.workflow(req, res);

  workflow.on('validate', function() {
    if (!req.body.address || !req.body.address.match(/^[a-z][a-z0-9]*$/)) {
      workflow.outcome.errfor.address = 'invalid';
    }

    if (workflow.hasErrors()) {
      return workflow.emit('response');
    }

    workflow.emit('patchAddress');
  });

  workflow.on('patchAddress', function() {
    var options = { 'new': true, select: "address" };

    req.app.db.models.Settings.findByIdAndUpdate(req.user.roles.settings.id, {address: req.body.address}, options, function(err, settings) {
      if (err) {
        return workflow.emit('exception', err);
      }

      workflow.outcome.settings = settings;
      return workflow.emit('response');
    });
  });

  workflow.emit('validate');
};

exports.update = function(req, res, next){
  var workflow = req.app.utility.workflow(req, res);

  workflow.on('validate', function() {

    if (req.body.language) {
      if (enumSystemLanguages.indexOf(req.body.language) === -1) {
        workflow.outcome.errfor.language = 'invalid';
      }
    }

    if (workflow.hasErrors()) {
      return workflow.emit('response');
    }

    workflow.emit('patchSettings');
  });

  workflow.on('patchSettings', function() {
    req.app.db.models.Settings.findById(req.user.roles.settings.id, settingsSelect.join(" ")).exec(function(err, settings) {
      if (err) {
        return workflow.emit('exception', err);
      }
      
      if (req.body.language) {
        settings.language = req.body.language;
      }

      if (req.body.pageView) {
        if(req.body.pageView.leftMenu) {
          for (var k in req.body.pageView.leftMenu) {
            if (k in settings.pageView.leftMenu) {
              settings.pageView.leftMenu[k] = !!req.body.pageView.leftMenu[k];
            }
          }
        }
        
        if (req.body.pageView.rightBlocks) {
          var rightBlocks = [];
          
          for (var k in req.body.pageView.rightBlocks) {
            var b = req.body.pageView.rightBlocks[k];
            if (enumRightBlocks.indexOf(b) > -1) {
              rightBlocks.push(b);
            }
          }
          
          if (rightBlocks.length == enumRightBlocks.length) {
            settings.pageView.rightBlocks = rightBlocks;
          }
        }
      }
      
      if (req.body.trade) {
        for (var k in req.body.trade) {
          if (k in settings.trade) {
            settings.trade[k] = !!req.body.trade[k];
          }
        }
      }
      
      if (req.body.privacy) {
        for (var k in req.body.privacy) {
          var p = req.body.privacy[k];
          if ((k in settings.privacy) && enumPrivacy.indexOf(p) > -1) {
            settings.privacy[k] = p;
          }
        }
      }

      var options = { 'new': true, select: settingsSelect.join(" ") };
  
      req.app.db.models.Settings.findByIdAndUpdate(req.user.roles.settings.id, settings, options, function(err, settings) {
        if (err) {
          return workflow.emit('exception', err);
        }
  
        workflow.outcome.settings = settings;
        return workflow.emit('response');
      });
    });
  });

  workflow.emit('validate');
};