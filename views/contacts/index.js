'use strict';

exports.init = function(req, res, next){
  res.render('contacts/index');
};

exports.button = function(req, res) {
  res.render('contacts/button');
};

exports.getContacts = function(req, res, next) {
  var workflow = req.app.utility.workflow(req, res);

  workflow.outcome.contacts = false;
    
  req.app.db.models.Contact.find({ $or: [ {user0: req.user.id}, {user1: req.user.id} ]}).lean().exec(function(err, contacts) {
    if (err) {
      return workflow.emit('exception', err);
    } else if (!contacts) {
      return workflow.emit('response');
    }
    
    var users = [];
    contacts.map(function(c) { users.push(c.user0 == req.user.id ? c.user1 : c.user0); });

    req.app.db.models.User.find({_id: { $in: users }}).select('num roles timeLastVisit').populate('roles.account', "name birthday showBirthday showBirthdayOnlyMonthAndDay contactsCity works").populate('roles.settings', "address").lean().exec(function(err, users) {
      if (err) {
        return workflow.emit('exception', err);
      } else {
        users.map(function(user, index) {
          var d_format = res.__('dateFormat');
          var momentjs = require('moment');
          
          user.account = user.roles.account;
          user.address = user.roles.settings.address;
          delete user.roles;
          
          var a = user.account;
          if (a.showBirthday == 'yes') {
            if (a.showBirthdayOnlyMonthAndDay == 'yes') {
              d_format = d_format.replace(/\.y|y/gi, "");
            }
            a.birthday = momentjs(a.birthday).format(d_format.toUpperCase());
          } else {
            a.birthday = "";
          }
          
          user.online = 

          contacts[index].user = user;
        });
      }
      
      workflow.outcome.contacts = contacts;
      return workflow.emit('response');        
    });
  });
};

exports.addContact = function(req, res, next) {
  var workflow = req.app.utility.workflow(req, res);

  workflow.on('validate', function() {
    var num = parseInt(req.body.num);
    
    if (typeof(num) === 'undefined') {
      workflow.outcome.errfor.num = 'invalid';
      return workflow.emit('response');
    }
    else {
      req.app.db.models.User.findOne({
        'num': req.body.num
      }).select('num roles').populate('roles.settings').exec(function(err, user) {
        if (err || !user) {
          workflow.outcome.errfor.num = 'invalid';
        }
        else {
          var settings = user.roles.settings;
          workflow.outcome.userTo = user.id;
          workflow.outcome.userNum = user.num;
          // @TODO: check privacy
        }

        if (workflow.hasErrors()) {
          return workflow.emit('response');
        }

        workflow.emit('addContact');
      });
    }
  });

  workflow.on('addContact', function() {
    var fieldsToSet = {};
    
    var idn0 = parseInt((workflow.outcome.userTo+"").match(/[0-9]/g).join(""));
    var idn1 = parseInt((req.user.id+"").match(/[0-9]/g).join(""));
    
    if (idn1 > idn0) {
      fieldsToSet.user0 = workflow.outcome.userTo;
      fieldsToSet.user1 = req.user.id;
    } else {
      fieldsToSet.user0 = req.user.id;
      fieldsToSet.user1 = workflow.outcome.userTo;
    }
    
    req.app.db.models.Contact.findOne(fieldsToSet).exec(function(err, contact) {
      if (err || !contact) {
        if (fieldsToSet.user0 == workflow.outcome.userTo) {
          fieldsToSet.relation1 = 1;
        } else {
          fieldsToSet.relation0 = 1;
        }

        req.app.db.models.Contact.create(fieldsToSet, function(err, newContact) {
          if (err) {
            return workflow.emit('exception', err);
          }
          
          newContact.userId = req.user.id;
    
          workflow.outcome.contact = newContact;
          workflow.emit('response');
        });
      } else {
        var options = { 'new': true, select: 'relation0 relation1' };

        if (fieldsToSet.user0 == workflow.outcome.userTo) {
          fieldsToSet.relation1 = 1;
        } else {
          fieldsToSet.relation0 = 1;
        }
        
        delete fieldsToSet.user0;
        delete fieldsToSet.user1;
        
        req.app.db.models.Contact.findByIdAndUpdate(contact._id, fieldsToSet, options, function(err, contact) {
          if (err) {
            return workflow.emit('exception', err);
          }
    
          workflow.outcome.contact = contact;
          return workflow.emit('response');
        });
      }
    });
  });

  workflow.emit('validate');
};

exports.delContact = function(req, res, next) {
  var workflow = req.app.utility.workflow(req, res);
  
  workflow.on('validate', function() {
    req.app.db.models.Contact.findById(req.params.id).exec(function(err, contact) {
      if (err || !contact) {
        workflow.outcome.errors.push('Not found contact.');
        return workflow.emit('response');
      }
      
      if (contact.user0 != req.user.id && contact.user1 != req.user.id) {
        workflow.outcome.errors.push('You do not own.');
        return workflow.emit('response');
      }
      
      workflow.emit('deleteContact');
    });
  });

  workflow.on('deleteContact', function() {
    req.app.db.models.Contact.findByIdAndRemove(req.params.id, function(err, contact) {
      if (err) {
        return workflow.emit('exception', err);
      }
      
      return workflow.emit('response');
    });
  });

  workflow.emit('validate');
};