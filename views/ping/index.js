'use strict';

exports.init = function(req, res){
  var workflow = req.app.utility.workflow(req, res);

    if (req.isAuthenticated()) {
        workflow.on('setTimeLastVisit', function() {
            req.app.db.models.User.findByIdAndUpdate(req.user.id, {timeLastVisit: Date.now()}, {select: ""}, function(err) {
                if (err) {
                    return workflow.emit('exception', err);
                }
          
                return workflow.emit('response');
            });
        });
        
        workflow.emit('setTimeLastVisit');
    } else {
        workflow.emit('response');
    }
};
